require('jquery')

$( document ).ready(function() {
    // Open popup setting
    $('.buttonSetting').click(function(){
        $('.topBarSettingPopup').toggleClass('topBarSettingPopupShow')
    })

    // Close popup
    $('.buttonClose').click(function(){
        $('.topBar').removeClass('topBarShow')
        $('.topBarShops').removeClass('topBarShopsShow')
        $('.topBarLocation').removeClass('topBarLocationShow')
    })
    // Open shops
    $('.js-openShops').click(function () {
        $('.topBarShops').toggleClass('topBarShopsShow')
        if($('.topBarLocation').hasClass('topBarLocationShow')) {
            $('.topBarLocation').removeClass('topBarLocationShow')
        }
    })
    // Open Location
    $('.js-openLocation').click(function () {
        $('.topBarLocation').toggleClass('topBarLocationShow')
    })
    // Show topBar after 20sec
    setTimeout(function() {
        if (!$(".topBar").hasClass('topBarShow')) {
           $(".topBar").addClass('topBarShow')

        }
    }, 3000)

    $(document).scroll(function() {
      var y = $(this).scrollTop();
      var height = $(window).height()
      if (y > height) {
        $(".topBar").addClass('topBarShow')
      }
    });
});
